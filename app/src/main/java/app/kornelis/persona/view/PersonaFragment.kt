package app.kornelis.persona.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.kornelis.persona.R
import app.kornelis.persona.binder.Command
import app.kornelis.persona.binder.DataEvent
import com.google.android.material.snackbar.Snackbar
import org.greenrobot.eventbus.EventBus
import app.kornelis.persona.binder.MessageEvent
import app.kornelis.persona.model.Persona
import app.kornelis.persona.model.RandomUser
import com.pixplicity.sharp.Sharp
import kotlinx.android.synthetic.main.fragment_persona.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.MessageFormat

/**
 * Created by Kees Marijs, November 2019.
 */
class PersonaFragment : Fragment() {

    private lateinit var rootView: View

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_persona, container, false)
    }

    override fun onResume() {
        super.onResume()

        fabNewPersona.setOnClickListener {
            Snackbar.make(rootView, R.string.notification_create_new_persona, Snackbar.LENGTH_SHORT)
                .setAction("Action", null)
                .show()
            EventBus.getDefault().post(MessageEvent(Command.GetPersonaData))
        }
    }

    /**
     * Listening to the vine.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDataEvent(event: DataEvent) {
        showUserDetails(event.randomUser)

        val url: String = MessageFormat.format("https://avatars.dicebear.com/v2/{0}/{1}.svg", event.randomUser.results[0].gender, event.randomUser.results[0].login.username)
        SVGUtils.fetchSvg(context, url, ivAvatar)
    }

    /**
     * Views are bound,
     * here is the User,
     * Let's tie them together.
     */
    private fun showUserDetails(randomUser: RandomUser) {
        val persona: Persona = randomUser.results[0]
        // Name & gender
        tvName.text = MessageFormat.format("{0} {1}, {2}", persona.name.title, persona.name.last, persona.name.first)
        tvGender.text = persona.gender
        tvAge.text = MessageFormat.format("{0}: {1} ({2})", getString(R.string.age), persona.dob.age, persona.dob.date.substring(0, 4))
        // Location
        tvStreet.text = MessageFormat.format("{0} {1}", persona.location.street.name, persona.location.street.number)
        tvCity.text = persona.location.city
        tvState.text = persona.location.state
        tvCountry.text = persona.location.country
        tvPostcode.text = persona.location.postcode
        // Contact
        tvUsername.text = persona.login.username
        tvEmail.text = persona.email
        tvPhone.text = persona.phone
        tvMobile.text = persona.cell
    }

    /**
     * Let's show the user the generated Avatar.
     */
    private fun showUserAvatar(svgImage: String) {
        Sharp.loadString(svgImage).into(ivAvatar)
    }
}