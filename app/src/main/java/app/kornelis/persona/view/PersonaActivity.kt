package app.kornelis.persona.view

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import app.kornelis.persona.R
import app.kornelis.persona.binder.Command
import app.kornelis.persona.model.VolleyService
import org.greenrobot.eventbus.EventBus
import app.kornelis.persona.binder.MessageEvent
import com.google.android.material.snackbar.Snackbar
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe

/**
 * Created by Kees Marijs, November 2019.
 */
class PersonaActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var drawerLayout: DrawerLayout
    lateinit var volleyService: VolleyService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        EventBus.getDefault().register(this)

        drawerLayout = findViewById(app.kornelis.persona.R.id.drawer_layout)
//        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(app.kornelis.persona.R.id.navHostFragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        appBarConfiguration = AppBarConfiguration(
//            setOf(
//                R.id.nav_home,
//                R.id.nav_gallery,
//                R.id.nav_slideshow,
//                R.id.nav_tools,
//                R.id.nav_share,
//                R.id.nav_send
//            ), drawerLayout
//        )
//        setupActionBarWithNavController(navController, appBarConfiguration)
//        navView.setupWithNavController(navController)
        volleyService = VolleyService(applicationContext)
    }

    public override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.navHostFragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    /**
     * Listening to stuff going on.
     * Some things we want to react to.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {/* Do something */
        when (event.command) {
            Command.GetPersonaData -> {
                volleyService.getNewPersonaData()
            }
            else -> {
                Snackbar.make(drawerLayout, R.string.notification_connection_error, Snackbar.LENGTH_SHORT)
                    .setAction("Action", null)
                    .show()
            }
        }
    }
}
