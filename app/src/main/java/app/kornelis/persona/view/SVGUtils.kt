package app.kornelis.persona.view

import com.pixplicity.sharp.Sharp
import android.content.Context
import android.widget.ImageView
import androidx.annotation.Nullable
import okhttp3.*
import java.io.IOException

/**
 * Helper class to load SVG image from url to image view.
 * From: https://muetsch.io/how-to-load-svg-into-imageview-by-url-in-android.html
 */
object SVGUtils {
    private var httpClient: OkHttpClient? = null

    fun fetchSvg(@Nullable context: Context?, url: String, target: ImageView) {
        if (httpClient == null) {
            // Use cache for performance and basic offline capability
            httpClient = OkHttpClient.Builder()
                .cache(Cache(context?.getCacheDir(), 5 * 1024 * 1014))
                .build()
        }

        val request = Request.Builder().url(url).build()
        httpClient!!.newCall(request).enqueue(object : Callback {
            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                val stream = response.body()?.byteStream()
                Sharp.loadInputStream(stream).into(target)
                stream?.close()
            }

            override fun onFailure(call: Call, e: IOException) {
//                target.setImageDrawable(R.mipmap.ic_launcher)
            }

        })
    }
}