package app.kornelis.persona.model

import android.content.Context
import app.kornelis.persona.binder.Command
import app.kornelis.persona.binder.DataEvent
import app.kornelis.persona.binder.MessageEvent
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import org.greenrobot.eventbus.EventBus

/**
 * Created by Kees Marijs, November 2019.
 * We need a connection to a server, with Volley.
 * And we shall call him VolleyService, and he shall serve us well.
 * From: https://github.com/Kotlin/kotlin-examples/tree/master/gradle/android-volley/app/src/main/java/org/example/kotlin/volley
 */
class VolleyService (context: Context) {

    private val URL_RANDOM_USER = "https://randomuser.me/api/"

    private val gson = Gson()
    private lateinit var context: Context

    val requestQueue: RequestQueue by lazy { Volley.newRequestQueue(context) }

    /**
     * Let's get a random RandomUser from Randomuser.me
     */
    public fun getNewPersonaData() {
        val request = StringRequest(Request.Method.GET, URL_RANDOM_USER, { response ->
                try {
                    val randomUser: RandomUser = gson.fromJson(response, RandomUser::class.java)
                    EventBus.getDefault().post(DataEvent(randomUser))

                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                    EventBus.getDefault().post(MessageEvent(Command.OnError))
                }
            },
            {
                EventBus.getDefault().post(MessageEvent(Command.OnError))
            })

        requestQueue.add(request)
    }
}