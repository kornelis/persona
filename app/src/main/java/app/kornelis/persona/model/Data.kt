package app.kornelis.persona.model

/**
 * Created by Kees Marijs, November 2019.
 */

data class RandomUser(
    val results: Array<Persona>,
    val info: Info) {

    /**
     * Overriding functions as recommended by Android Studio.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RandomUser

        if (!results.contentEquals(other.results)) return false
        if (info != other.info) return false

        return true
    }

    override fun hashCode(): Int {
        var result = results.contentHashCode()
        result = 31 * result + info.hashCode()
        return result
    }
}

data class Persona(
    val gender: String,
    val name: Name,
    val location: Location,
    val email: String,
    val login: Login,
    val dob: DaB,
    val registered: Registered,
    val phone: String,
    val cell: String,
    val nat: String)

data class Name(
    val title: String,
    val first: String,
    val last: String)

data class Location(
    val street: Street,
    val city: String,
    val state: String,
    val country: String,
    val postcode: String,
    val coordinates: Coordinates,
    val timezone: TimeZone)

data class Street(
    val number: String,
    val name: String)

data class Coordinates(
    val latitude: String,
    val longitude: String)

data class TimeZone(
    val offset: String,
    val description: String)

data class Login(
    val uuid: String,
    val username: String,
    val password: String,
    val salt: String,
    val md5: String,
    val sha1: String,
    val sha256: String)

data class DaB(
    val date: String,
    val age: String)

data class Registered(
    val date: String,
    val age: String
)

data class Info(
    val seed: String,
    val results: String,
    val page: String,
    val version: String)
