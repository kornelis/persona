package app.kornelis.persona.binder

import app.kornelis.persona.model.RandomUser

/**
 * Message Events can be several kinds of Commands of things to do.
 */
enum class Command {
    GetPersonaData,
    GetPersonaImage,
    OnError
}

/**
 * Send a Message to do something.
 */
class MessageEvent(val command: Command)

/**
 * Returning data to the whom ever is listening.
 */
class DataEvent(val randomUser: RandomUser)
