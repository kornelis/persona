package app.kornelis.persona

import app.kornelis.persona.model.RandomUser
import com.google.gson.Gson
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.IOException
import java.io.InputStream

/**
 * Test the parsing or the data from the server.
 */
class PersonaUnitTest {

    private val gson = Gson()

    @Test
    fun load_json_string() {
        val randomUserString = get_random_user()
        assertTrue(randomUserString != null)
        assertTrue(randomUserString!!.isNotEmpty())
    }

    @Test
    fun parse_json_string() {
        val randomUserString = get_random_user()

        val randomUser: RandomUser = gson.fromJson(randomUserString, RandomUser::class.java)
        assertTrue(randomUser != null)
    }

    @Test
    fun ensure_json_parsed_all_values_correctly() {
        val randomUserString = get_random_user()

        val randomUser: RandomUser = gson.fromJson(randomUserString, RandomUser::class.java)
        val user = randomUser.results.get(0)

        assertTrue(user != null)
        assertTrue(user.name.first.isNotEmpty())
        assertTrue(user.name.last.isNotEmpty())
        assertTrue(user.name.title.isNotEmpty())

        assertTrue(user.gender.isNotEmpty())
        assertTrue(user.dob.age.isNotEmpty())
        assertTrue(user.dob.date.isNotEmpty())

        assertTrue(user.phone.isNotEmpty())
        assertTrue(user.email.isNotEmpty())
        assertTrue(user.cell.isNotEmpty())
    }

    private fun get_random_user() : String? {
        try {
            val inputStream: InputStream = this.javaClass.classLoader!!.getResourceAsStream("RandomUser.txt")
            val length = inputStream.available()
            val buffer = ByteArray(length)
            inputStream.read(buffer)

            return String(buffer)

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }
}
